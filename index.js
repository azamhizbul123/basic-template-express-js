const express = require('express');
const port = 3000
const app = express()
// let session = require('express-session')
const User = require('./routes/User.Route')
const Utils = require('./routes/Utils.Route')

// using express session
// app.use(session({
//     secret: process.env.SESSION_SECRET,
//     saveUninitialized: true,
//     resave: false,
//     cookie: {
//         httpOnly:true,
//         maxAge:1000 * 60 * 60 * 24
//     }
// }))

app.use('/user', User);
app.use('/utils', Utils)

app.listen(port, ()=> {
    console.log(`Serve listening at http://0.0.0.0:${port}`)
})