const db = require('../utils/DbConnection')

const getUser = (callback) => {
    // with callback function

    // db.from('user')
    // .select('*')
    // .then(e => {
    //     callback(e)
    // })

    return db.from('user')
    .select('*')
}

const createUserM = (data) => {
    return db('user').insert({username:data.username, password:data.password})
}

const getDataLimitFromTable = (data, tableName, limit) => {
    return db(tableName)
    .where(data)
    .limit(limit)
}

module.exports = {getUser, createUserM, getDataLimitFromTable}