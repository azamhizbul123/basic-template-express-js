const bcrypt = require("bcrypt");
const {
  getUser,
  createUserM,
  getDataLimitFromTable,
} = require("../models/User.Model");
const { encrypt, decrypt } = require("../utils/Encrypt");
const { generateTokenUser } = require("../utils/Token");


const getUserC = async (req, callback) => {
  // with callback function
  // getUser( (e) => {
  //     callback(200, e)
  // })
  const x = await getUser();
  return { code: 200, data: x };
};

const createUser = async (data) => {
  const salt = await bcrypt.genSalt(10);
  data.password = await bcrypt.hash(data.password, salt);

  //const insert = await createUserM(data)
  return createUserM(data)
    .then((e) => {
      return { code: 200, data: "success inserted" };
    })
    .catch((err) => {
      return { code: 500, data: err.detail };
    });
};

const authUser = async (data) => {
  try {
    const paramData = { username: data.username };
    if (data.username == null || "" || undefined)
      return { code: 400, data: "All input is required" };

    const dataUser = await getDataLimitFromTable(paramData, "user", 1);

    if (
      dataUser &&
      (await bcrypt.compare(data.password, dataUser[0].password))
    ) {
      const userEncrypt = encrypt(paramData.username)
      paramData.username = userEncrypt.encryptedData
      const tokenUser = await generateTokenUser(paramData);
      return { code: 200, data: tokenUser };
    }

    return { code: 400, data: "Invalid Credentials" };
  } catch (err) {
    console.log(err);
  }
};

module.exports = { getUserC, createUser, authUser };
